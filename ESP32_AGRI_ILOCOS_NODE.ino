
// Please select the corresponding model

//#define SIM800L_IP5306_VERSION_20190610
// #define SIM800L_AXP192_VERSION_20200327
// #define SIM800C_AXP192_VERSION_20200609
#define SIM800L_IP5306_VERSION_20200811
// Select your modem:
#define TINY_GSM_MODEM_SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb
// BME280 pins
#define I2C_SDA_2            18
#define I2C_SCL_2            19

#include "utilities.h"
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>



// Set serial for debug console (to the Serial Monitor, default speed 115200)
#define SerialMon Serial

// Set serial for AT commands (to the module)
// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial1

// See all AT commands, if wanted
#define DUMP_AT_COMMANDS

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon

// Add a reception delay - may be needed for a fast processor at a slow baud rate
// #define TINY_GSM_YIELD() { delay(2); }

// Define how you're planning to connect to the internet
#define TINY_GSM_USE_GPRS true
#define TINY_GSM_USE_WIFI false

// set GSM PIN, if any
#define GSM_PIN ""

#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, SerialMon);
  TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif
TinyGsmClient client(modem);
PubSubClient mqtt(client);

TwoWire I2CBME = TwoWire(1);
Adafruit_BME280 bme;
#define SEALEVELPRESSURE_HPA (1013.25)

int ledStatus = LOW;

uint32_t lastReconnectAttempt = 0;
unsigned long lastpayload = 0;


// Your GPRS credentials, if any
const char apn[] = "internet.globe.com.ph";
const char gprsUser[] = "";
const char gprsPass[] = "";

//MQTT INFORMATION
const char* mqttServer = "139.59.107.22";
const int mqttPort = 44443;
const char* mqttUser = "aruga-agri-user";
const char* mqttPassword = "Xyz0712302Abc";

const char *topicLed = "1rl-test/led";
const char *topic_callback = "/Aruga/Agri/IlocosSur/1";
String topic_main_payload = "Aruga/Agri/IlocosSur/";
String topic_command;

float humidity; 
float temperature;
float altitude;
float pressure;
int dev_id = 269;
uint32_t chipId = 0;

void mqttCallback(char *topic, byte *payload, unsigned int length)
{
    char buff_p[length];
    for (int i = 0; i < length; i++)
    {
    Serial.print((char)payload[i]);
    buff_p[i] = (char)payload[i];
    }
    buff_p[length] = '\0';
    String msg_p = String(buff_p);
    Serial.println(msg_p);
    if(msg_p=="restart"){
      ESP.restart();
    }
    
}

boolean mqttConnect()
{
    String ID = String(chipId, HEX);
    boolean status = mqtt.connect(ID.c_str(), mqttUser, mqttPassword );

    // Or, if you want to authenticate MQTT:
    //boolean status = mqtt.connect("GsmClientName", "mqtt_user", "mqtt_pass");

    if (status == false) {
        SerialMon.println(" fail");
        return false;
    }
    SerialMon.println(" success");
    mqtt.subscribe(topic_command.c_str());
    return mqtt.connected();
}


void setup()
{
    // Set console baud rate
    SerialMon.begin(115200);
    
    I2CBME.begin(I2C_SDA_2, I2C_SCL_2, 400000);
    if (!bme.begin(0x76, &I2CBME)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    }
    delay(10);

    setupModem();

    SerialMon.println("Wait...");
    for(int i=0; i<17; i=i+8) {
    chipId |= ((ESP.getEfuseMac() >> (40 - i)) & 0xff) << i;
    }
    
    topic_main_payload +=String(chipId, HEX);

    topic_command += topic_main_payload;
    topic_command +="/command";
    SerialMon.println(topic_command);
    // Set GSM module baud rate and UART pins


   
    
    SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);

    delay(6000);

    // Restart takes quite some time
    // To skip it, call init() instead of restart()
    SerialMon.println("Initializing modem...");
    modem.restart();
    // modem.init();

    String modemInfo = modem.getModemInfo();
    SerialMon.print("Modem Info: ");
    SerialMon.println(modemInfo);

#if TINY_GSM_USE_GPRS
    // Unlock your SIM card with a PIN if needed
    if ( GSM_PIN && modem.getSimStatus() != 3 ) {
        modem.simUnlock(GSM_PIN);
    }
#endif

    SerialMon.print("Waiting for network...");
    if (!modem.waitForNetwork()) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isNetworkConnected()) {
        SerialMon.println("Network connected");
    }

    // GPRS connection parameters are usually set after network registration
    SerialMon.print(F("Connecting to "));
    SerialMon.print(apn);
    if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isGprsConnected()) {
        SerialMon.println("GPRS connected");
    }

    // MQTT Broker setup
    mqtt.setServer(mqttServer, mqttPort);
    mqtt.setCallback(mqttCallback);
}

void loop()
{

    if (!mqtt.connected()) {
        SerialMon.println("=== MQTT NOT CONNECTED ===");
        // Reconnect every 10 seconds
        uint32_t t = millis();
        
        if (t - lastReconnectAttempt > 30000L) {
            lastReconnectAttempt = t;
            if (mqttConnect()) {
                lastReconnectAttempt = 0;
            }
        }
        delay(100);
        return;
    }
    uint32_t a = millis();
      if (a - lastpayload > 300000L) {
        readValues();
        delay(1000);
        createJsonString();
        String msg = createJsonString();
        
        mqtt.publish(topic_main_payload.c_str(), msg.c_str());
        lastpayload = a;
      }
      
      mqtt.loop();
}

void readValues() {
    //temperature=random(15, 25);
    //humidity=random(20, 70);

    temperature = bme.readTemperature();
    pressure = (bme.readPressure() / 100.0F);
    altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
    humidity = bme.readHumidity();

    
   
}

String createJsonString() 
{  
   String data = "{";
      data+="\"device_id\":\"";
      data+=String(chipId, HEX);
      data+="\",";
      data+=" \"temperature\":\"";
      data+=String(temperature);
      data+="\",";
      data+=" \"humidity\":\"";
      data+=String(humidity);
      data+="\",";
      data+=" \"altitude\":\"";
      data+=String(altitude);
      data+="\",";
      data+=" \"pressure\":\"";
      data+=String(pressure);
      data+="\"}";
  
  return data;
}
